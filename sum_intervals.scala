// create (sum, max) where `sum` is the running sum and `max` is the largest span endpoint reached
// so far
// since the list has been sorted, we know the new span's left endpoint is the larger of `max`
// and the incoming span's left endpoint
// but the incoming span's right endpoint could be less than `max`, too, so the new span's right
// endpoint is the larger of `max` and the incoming span's right endpoint

def sumOfIntervals(intervals: List[(Int, Int)]): Int = 
  intervals
    .sorted
    .fold(0, Int.MinValue)
         ((t1: (Int, Int), t2: (Int, Int)) => 
            (t1._1 + ((t1._2).max(t2._2) - (t1._2).max(t2._1)), (t1._2).max(t2._2)))
    ._1